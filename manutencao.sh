#!/bin/bash

echo '****** RMPFUSION *******'
sudo dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
echo '****** END Install ******'

echo '******* plasma-systemmonitor *******'
sudo dnf -y install plasma-systemmonitor
echo '****** END Install ******'

echo '******* htop *******'
sudo dnf -y install htop
echo '****** END Install ******'

echo '****** Libre Office para portugues brasil *******'
sudo dnf -y install libreoffice libreoffice-langpack-pt-BR
echo '****** End Install *******'

echo '******* GParted *******'
sudo dnf -y install gparted
echo '******* End Install *******'

echo '******* GSmartControl *******'
sudo dnf -y install gsmartcontrol
echo '******* End Install *******'

echo '******* FileZilla *******'
sudo dnf -y install filezilla
echo '******* End Install *******'

echo '******* Dolphin *******'
sudo dnf -y install dolphin
echo '****** END Install ******'

echo '******* openssh *******'
sudo dnf -y install openssh
echo '****** END Install ******'
